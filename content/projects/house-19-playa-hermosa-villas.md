---
categories: Residential
title: 'House #19 @ Playa Hermosa Villas'
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1586388251/Villa_19_fpwhta.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230634/1_residential__casa__193_FACHADA_PRINCIPAL_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230635/1_residential__casa__191_DETALLE_FACHADA_LATERAL_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230635/1_residential__casa__192_DETALLE_FACHADA_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230628/1_residential__casa__19IMG_20160615_121118_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230625/1_residential__casa__19IMG_20160615_120808_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230635/1_residential__casa__19IMG_20160615_121510_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230627/1_residential__casa__19IMG_20160615_122306_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230635/1_residential__casa__19IMG_20160615_122203_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230628/1_residential__casa__19IMG_20160615_121815_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230633/1_residential__casa__19IMG_20160615_122500_jpg-Optimized.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230632/1_residential__casa__19IMG_20160615_121430_jpg-Optimized.jpg
youtube_url: ''
weight: 99
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
The client´s main requirement was to open the design to the tropical surroundings; with all the rooms enjoying the property´s privileged views to the coast. Thus, the best architectural solution was to build in three levels, not only for taking advantage of the view but also for minimizing the construction footprint. The ground level, that acts as a semi-basement, accommodates the carport and the main entrance, the second level, which actually is level with the ground too, has the social area and the third floor accommodating the bedrooms.