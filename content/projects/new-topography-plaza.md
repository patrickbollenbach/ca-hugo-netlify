---
categories: Urban Design
title: New Topography Plaza
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228509/3_Parque_Berrera_JPG.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228509/3_Parque_Berrera_JPG.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228514/new_topography_plaza4_ACCESO_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228514/new_topography_plaza5__DETALLE_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228522/new_topography_plaza6_DETALLE_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228522/new_topography_plaza7_DETALLE_ESCULTURA_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228506/new_topography_plaza8_Planta_jpg.jpg
youtube_url: ''
weight: 2
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
