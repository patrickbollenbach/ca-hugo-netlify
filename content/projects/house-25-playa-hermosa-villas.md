---
categories: Residential
title: 'House #25 @ Playa Hermosa Villas'
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1586387993/Villa_25_rkihvq.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231035/1_residential__casa__251_DETALLE_ENTRADA_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231053/1_residential__casa__252_FACHADA_LATERAL_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231087/1_residential__casa__253_FACHADA_PRINCIPAL_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231052/1_residential__casa__254_ESCALERAS_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231040/1_residential__casa__255_SALA_Y_PATIO_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231041/1_residential__casa__257_DETALLE_COMEDOR_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231035/1_residential__casa__258_COCINA_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231048/1_residential__casa__259_DETALLE_LAMPARA_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231054/1_residential__casa__2510_SALA_TV_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231068/1_residential__casa__2511_DORMITORIO_SECUNDARIO_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231080/1_residential__casa__2513_DORMITORIO_PRINCIPAL_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231065/1_residential__casa__2514_BAN_O_PRINCIPAL_DUCHA_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231086/1_residential__casa__2516_BALCON_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231076/1_residential__casa__2517_PATIO_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231007/1_residential__casa__2518_DETALLE_BALCON_jpg.jpg
youtube_url: https://www.youtube.com/watch?v=bCuSz89YiGY&fbclid=IwAR0SJJhR0Yn19EW8Zi8LgcWLJrmCt_08ZY-8osYhOSujgMN3XXa_jxqJrnQ
weight: 100
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
Being at the entrance of a dense residential complex turns the quest for privacy one of the main concepts of the design; in fact, this is the reason why the house gives the back to the busy exterior while time opening itself to the inside, to the inner garden, to that private tropical paradise that fills with light and fresh air all the rooms in the house.