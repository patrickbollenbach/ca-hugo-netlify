---
categories: Residential
title: Bogard Residence
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228340/1_residential__bogard_residence02_jpg.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228355/1_residential__bogard_residence03_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228354/1_residential__bogard_residence04_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228352/1_residential__bogard_residence05_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228341/1_residential__bogard_residence06_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228348/1_residential__bogard_residence07_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228353/1_residential__bogard_residence09_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228356/1_residential__bogard_residence11_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228344/1_residential__bogard_residence29_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228343/1_residential__bogard_residence19_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228344/1_residential__bogard_residence20_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559228351/1_residential__bogard_residence16_1_jpg.jpg
youtube_url: ''
weight: 90
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
Located in a small lot in a dense residential community close to the beach, its architecture aim is to make the most efficient use of the land, by raising its construction above the ground both to fulfill regulations requirements and for giving a lightness sensation to it; lightness enhanced by the cantilevered void on the façade protruding the first floor that acts also as roofing for the carport at the entrance.