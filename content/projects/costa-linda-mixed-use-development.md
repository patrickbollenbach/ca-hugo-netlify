---
categories: Urban Design
title: 'Costa Linda: mixed use development -proposal-'
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230940/costa_linda_mix_use_development6_jpg.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559231363/costa_linda_mix_use_development2_COSTA_LINDA_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230941/costa_linda_mix_use_development4_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230935/costa_linda_mix_use_development5_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230943/costa_linda_mix_use_development10_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230942/costa_linda_mix_use_development11_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230954/costa_linda_mix_use_development12_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230958/costa_linda_mix_use_development15_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230960/costa_linda_mix_use_development18_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230959/costa_linda_mix_use_development19_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230958/costa_linda_mix_use_development20_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230959/costa_linda_mix_use_development21_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230956/costa_linda_mix_use_development25_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230952/costa_linda_mix_use_development28_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230957/costa_linda_mix_use_development31_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230956/costa_linda_mix_use_development32_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230925/costa_linda_mix_use_developmenta_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230926/costa_linda_mix_use_developmentm_jpg.jpg
youtube_url: ''
weight: 
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
