---
categories: Hotels And Restaurants
title: Rancho Pacifico boutique hotel -renovation-
featured: true
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1586228197/Clubhouse12345_rdwcvy.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790191/4.7.Aerialviewdistant_krtpm8.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790194/4.6.Aerialviewmidrange_zzesb6.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790191/1.8.Aerialcloseup_amxoea.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790183/1.10.Hotelentrance_yb5bay.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790195/35.PoolsideBar_ay9adn.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790181/36.PoolsideTerrace_igecc0.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790145/15.Infinitypoolandrestaurant_naz3d9.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790184/14.Infinitypoolandgreenterrace_niukri.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790157/1.37.ReceptionDesk2_lvrfz4.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790187/1.24.Lounge3_bu0ugj.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790173/20Infinitypoolatsunset_vyrchu.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790187/22infinitypoolundereveningskies_ys560c.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790175/34.PoolandTreeMirrorImages_j0muiq.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790172/12.InfinityJacuzziandWhale_sTail_grkiy8.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790127/28.Luxurysuitebedandbath_llhoh7.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790103/32.LuxurySuitewithOceanView_qufsvt.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790108/29.LuxurySuiteBedroom_yyznra.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790177/30.LuxurySuiteSinks_nzjuf9.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790122/31.LuxurySuiteSoakingTubandOceanView_r4zqg8.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566790091/27.LuxurySuiteSoakingTub_hn3a7o.jpg
youtube_url: https://www.facebook.com/211564638926658/videos/2325518951038907/
second_youtube_url: https://vimeo.com/408512368
third_youtube_url: https://vimeo.com/408512383

---
The renovation of the Rancho Pacifico Boutique Hotel, on the hills overlooking the town of Uvita and the whale´s tale bay, perfectly exemplifies what is possible when the visions of the architect and the client align: an aim for a minimal intervention on the site while creating an architectural statement landmark.

A new club house, pool, lounge area and 2 new suites where added to the existing hotel infrastructure.

The dark palette chosen for the design is the perfect complement to all the greens of the surrounding tropical jungle and the blues of the sky, and enhanced with wooden custom made focal elements, such as the reception teak deck, teak beds and Guanacaste wood countertops in the bathrooms, bathrooms that opens up to a magnificent view of the jungle and the ocean down below.