---
categories: Residential
title: Wood Residence
featured: true
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1586387658/IMG-4233_ds9wyy.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566789351/0-2019-03-04_16.50_cokvgb.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566789465/2019-03-04_16.51_srqxk3.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566788959/0-WhatsApp_Image_2019-05-15_at_16.25.18_1_sq16rz.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566788942/0-WhatsApp_Image_2019-05-15_at_16.24.11_goop5x.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566788959/0-WhatsApp_Image_2019-05-15_at_16.25.17_ipqiqy.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566788959/WhatsApp_Image_2019-05-15_at_16.24.58_fyirge.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566788956/0-WhatsApp_Image_2019-05-15_at_16.25.03_qrzta0.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566788952/0-WhatsApp_Image_2019-05-15_at_16.24.57_1_t4ioon.jpg
youtube_url: https://www.youtube.com/watch?v=uSyXp3LPN98
weight: 5
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
