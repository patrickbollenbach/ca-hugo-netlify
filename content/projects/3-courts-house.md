---
categories: Residential
title: Three courts house
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786834/4157fcb6-c8ab-4501-a0d2-13ff1766e9d5_zusdrx.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786886/00321a50-c145-458c-9ebb-4d3e78c2c6fe_mtkyug.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786883/dd02f55d-2aa7-4b2a-9055-20bdc9e25b9d_ykydvr.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786884/f92185f7-97c2-4432-932c-5ba9442dd5cf_haei21.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786883/6d05b69b-ac7d-4c3d-a7ab-ba8e6deb86a5_gjsbk3.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786883/4fa11606-1c22-4ff2-a055-877f10daf3fe_lfxldq.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786883/59cc8208-72d0-49d8-8d03-88d47771b187_pcw4im.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786884/ae1dc731-693b-47d6-ba72-3e9716ed7f42_nayavw.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786883/0ea86009-7e5c-4f52-87f5-98665fff8111_q3nscb.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786884/6706a5b6-d6a3-4bcd-b32a-bafe72803c84_zb830l.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786883/b4e824eb-bbab-4562-950a-861bbb8b5367_fh3tsy.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1566786886/22661ca4-6d30-4369-b743-aca843da0bf3_u09fu5.jpg
youtube_url: https://www.youtube.com/watch?v=jXSF2_CPHok
weight: 2
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
Located in a residential development only steps away from the Marino Ballena National Park, the house was designed aiming for privacy and protection from direct sun exposure.

This was achieved by aligning its layout with the cardinal points, resulting in a tilted (in relation to the property lines) blind walled east façade and a west façade with generous overhangs facing the main court inside.

As soon as one comes in, the social area welcomes the spectator with a spacious and luminous room, with the bedrooms facing north and south, which are in the tropics the least exposed façades to the sun.

In the master bedroom, along with its privileged view to the pool, its ensuite bathroom has a lush private court with an outdoor shower and bathtub that gives the sensation of being one with the nature.

You can stay in this stunning house too! Simply by clicking on the following link:  [https://www.airbnb.co.cr/rooms/22884299?adults=2&source_impression_id=p3_1566585222_C8RSi8vz19NDk94S](https://www.airbnb.co.cr/rooms/22884299?adults=2&source_impression_id=p3_1566585222_C8RSi8vz19NDk94S "https://www.airbnb.co.cr/rooms/22884299?adults=2&source_impression_id=p3_1566585222_C8RSi8vz19NDk94S")