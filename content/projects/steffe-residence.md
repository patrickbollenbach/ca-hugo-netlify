---
weight: 70
categories: Residential
title: Steffe Residence -proposal-
featured: true
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1590720869/Steffes_oe68aw.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263655/1_mnsnvr.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263603/4_l9p8ah.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263604/12_sos5wj.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263603/8_cyyju6.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263583/10_gyvzt0.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263663/14_z6vldn.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263651/18_q3vdoi.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263578/21_ssmcln.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263572/22_mn4uso.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263572/23_gvzlfs.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263588/19_jxg41b.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263583/31_dkl3qj.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263656/32_offr3r.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1585263588/37_l8mvzp.jpg
youtube_url: ''
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
