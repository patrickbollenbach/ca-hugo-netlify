---
categories: Residential
title: DeShae Guesthouse -under construction-
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227756/1.residential.deshae-guesthouse1.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227759/1.residential.deshae-guesthouse2.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227755/1.residential.deshae-guesthouse5.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227758/1.residential.deshae-guesthouse3.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227758/1.residential.deshae-guesthouse6.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227757/1.residential.deshae-guesthouse4.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559227757/1.residential.deshae-guesthouse7.jpg
youtube_url: https://www.youtube.com/watch?v=uSp0iKgcVrE
weight: 80
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
