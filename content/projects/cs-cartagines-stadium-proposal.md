---
categories: Sports
title: CS Cartagines Stadium -proposal-
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230790/4_sports_cs_cartagines_stadium_proposal3_jpg.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230791/4_sports_cs_cartagines_stadium_proposal1_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230791/4_sports_cs_cartagines_stadium_proposal2_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230790/4_sports_cs_cartagines_stadium_proposal3_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230793/4_sports_cs_cartagines_stadium_proposal4_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230792/4_sports_cs_cartagines_stadium_proposal5_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230792/4_sports_cs_cartagines_stadium_proposal6_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230793/4_sports_cs_cartagines_stadium_proposal7_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230791/4_sports_cs_cartagines_stadium_proposal8_jpg.jpg
youtube_url: ''
weight: 
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
Designed in partnership with the architect Marco Navarro, the proposal for a new stadium for the C.S.Cartagines, the oldest active football soccer team in the country, was developed for a private's client that is looking forward to build world class infraestructure for the team and the area surrounding it.

A stadium with 15.000 seats, training fields, commercial, hotel and office facilities is part of the master plan for developing and creating a whole new hub in the city of Cartago.