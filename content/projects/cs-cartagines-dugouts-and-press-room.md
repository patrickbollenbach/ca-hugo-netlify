---
categories: Sports
title: CS Cartagines dugouts and press room
featured: false
project_featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230868/4_sports_cs_cartagines_dugouts9_jpg.jpg
project_gallery:
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230857/4_sports_cs_cartagines_dugouts2_1_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230860/4_sports_cs_cartagines_dugouts2_2_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230861/4_sports_cs_cartagines_dugouts3_1_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230893/4_sports_cs_cartagines_dugouts3_2_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230862/4_sports_cs_cartagines_dugouts4_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230892/4_sports_cs_cartagines_dugouts5_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230858/4_sports_cs_cartagines_dugouts6_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230862/4_sports_cs_cartagines_dugouts7_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230868/4_sports_cs_cartagines_dugouts8_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230868/4_sports_cs_cartagines_dugouts9_jpg.jpg
- https://res.cloudinary.com/dpoitlxyq/image/upload/v1559230864/4_sports_cs_cartagines_dugouts10_jpg.jpg
youtube_url: https://www.facebook.com/CartaginesCR/videos/1092918964083208/
weight: 
second_youtube_url: ''
third_youtube_url: ''
fourth_youtube_url: ''
fifth_youtube_url: ''

---
