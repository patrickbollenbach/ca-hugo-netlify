---
title: About
date: 2018-06-05T14:29:45.000+00:00
layout: single
type: page
featured_image: https://res.cloudinary.com/dpoitlxyq/image/upload/v1566779756/uvita-beach-iStock-687372014-1024x684_ar4rvj.jpg
team_member:
- name: Carlos Mata
  description: Architect
  photo: https://res.cloudinary.com/dpoitlxyq/image/upload/v1563241927/yo.jpg
- name: Guillermo Hernandez
  description: Structural Engineer
  photo: https://res.cloudinary.com/dpoitlxyq/image/upload/v1563241994/Guillermo.jpg
- name: Ignacio Delgado
  description: Electromechanical Engineer
  photo: ''
menu:
  main:
    identifier: About
    weight: 1

---
Born (1980) and raised in Cartago, 20 kilometers east of the country's capital, San Jose, Carlos Mata studied Industrial Design for two years and a half at the Technological Institute of Costa Rica before switching to Architecture, in the same university. After three years of professional practice in San Jose, he decides to start its own firm in 2013, basing its professional activity in the south Pacific coast of Costa Rica, a bustling area surrounded by beaches, tropical jungle and mountains overlooking the Pacific Ocean.

The firm's manifesto:

An aim for a tropical contemporary approach to the design

The spirit of the place and the spirit of the time as its leitmotiv

The search for timeless architecture

Seduction, sensorial an time experience

Open floor plan, spacial continuity